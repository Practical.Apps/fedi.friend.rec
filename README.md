***Summary***

This is a python script for generating follow recommendations for fediverse accounts.\
The program is interactive and offers several different options for saving and displaying data while running.

***Why***

The fediverse is a distributed social media network comprised of servers running free software connected via the ActivityPub protocol.\
This system offers many advantages over centralized alternatives such as Twitter but also creates certain unique challenges.\
One such challenge is that finding people to follow on the fediverse can be difficult, especially outside of your own instance.\
I would like to encourage wider adoption of the fediverse and to help users new and old to make more connections.\
I am happy to provide this work which will hopefully help you and others.

***How it works***

The idea of the program is to suggest accounts to follow based on who the given user already follows. 
The script pulls the list of accounts the user follows, and then cycles through them collecting the lists of who those users follow. 
The result is a large list of accounts which includes duplicates. Each duplicate entry in this larger list is an instance of "overlap." 
The recommendations are based on which accounts have the most overlap, any account with greater than average overlap is included.
The theory is that accounts who are followed by many accounts that you follow should be of interest to you.
The results are ordered by which accounts have the highest degree of overlap, descending to the (theoretically) least relevant.

***For your consideration***

Version Requirements\
Certain elements of this script require Python3 so do not try to run this script with earlier versions of Python.

Cached Lists\
The `cache.py` file is only included to show the format for the data, i.e. a dictionary of user(str):following_list(list) entries.
I have included my own small following list as a placeholder so the file is not completely blank.
If you don't intend to run the program more than once then using the cache is not necessary and you should choose "No" when prompted.
If you do want to cache data, the new list and dict are saved in the output file before the recommendations.

Timing and Rate Limiting\
You will notice in the code there are a couple instances of the time.sleep() function. 
This is to prevent the user of the script from getting rate limited for putting in requests too quickly.
Worth noting is that I have only encountered rate limiting on one server (noagendasocial.com). 
The rate limiting function may work differently on other servers or it may not be implemented at all. 
You may consider changing or eliminating these lines to save time if this is not an issue that affects you.
If you follow many accounts and leave these delays in place, be advised that the program will take a long time to complete.

***References/Attribution***

The following pages were used for reference in the creation of this script:

A handy way to sort dictionaries by their values came from:\
https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value

A very quick way to get the UTC time offset while taking DST into account came from:\
https://stackoverflow.com/questions/3168096/getting-computers-utc-offset-in-python
