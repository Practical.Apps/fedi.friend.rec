import time
import requests
# Disable insecure request warning or it may appear every time a request is made
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Ask whether to use cached data to file at the end
print("This program is able to import cached data to speed up the run time")
print("You need data in a dictionary named 'csfd' in a file named 'cache.py'")
use_cache = input('Do you want to use cached data? (y/n): ')
if use_cache in ['y', 'Y', 'yes']:
    ucb = True
    print("Data will be saved to file at the end of the program")
else:
    ucb = False
    print("Data will not be saved to file")
if ucb:
    from cache import csfd
print("Using cache, size of cache is: " + str(len(csfd)) + " accounts")

# Get the user either as a url or as a tag
print("Enter either the user URL e.g. https://instance.tld/users/username")
print("or the user tag e.g. username@instance.tld")
recfor = input("User: ")
if "/" in recfor:                                  # If a URL was given
    home = recfor.split(sep='/')[2]                # Record instance domain
else:                                              # If a tag was given
    home = recfor.split(sep='@')[1]                # Record instance domain and make into URL
    recfor = "https://" + recfor.split(sep='@')[1] + "/users/" + recfor.split(sep='@')[0]
    
# Ask whether to save the data to file at the end
save_data = input('Will you want to save any data to file? (y/n): ')
if save_data in ['y', 'Y', 'yes']:
    sfb = True
    print("Data will be saved to file at the end of the program")
else:
    sfb = False
    print("Data will not be saved to file")
    
# Create prune list for any dead accounts
prune = []
# Create header which will be constant
headers = {}
headers["Accept"] = "application/json"

### Crawl Follow List Function ###
def get_followers(user):
    next_page = user + "/following?page=1"
    total = gsize = i = 0
    curpage = endpage = "0"
    folcomp = False
    following = []
    global prune
    while not folcomp:
        time.sleep(2)
        try:
            resp = requests.get(next_page, headers=headers, verify=False, timeout=20)  # Send get request to server, no cert check, wait maximum 20s for slow response
        except:
            print("Error in get_followers with " + next_page)                   # If there was no response display error message
            print("Error: (C00) - Response not received")
            if next_page.endswith('=1') or next_page.endswith('=true'):             # If this error was on the first page
                prune.append(user)                                                      # add account to prune list 
            break                                                                   # End the loop without proceeding as there was no response
        if resp.status_code == 200:                                             # Successful response code, begin parsing data
            try:
                respd = resp.json()                                                 # Check that response includes json object
            except:
                print("Error: (C01) - " + next_page + ' did not return a json')
                if next_page.endswith('=1') or next_page.endswith('=true'):
                    prune.append(user)
                break                                                               # End the loop without proceeding as there is no data
            try:
                following += respd['orderedItems']                                  # Check for 'orderedItems' which is the following list
            except KeyError:
                print("Error: (C02) - " + next_page + " json did not contain 'orderedItems' element")
                break                                                               # End the loop as something is wrong (broken link)
            if len(respd['orderedItems']) == 0:                                     # This happens sometimes (next link on actual final page of following list)
                break                                                                   # Break because continuing will show incorrect page count (3 out of 2)
            curpage = next_page.split(sep="page=")[1]                               # Set the current page number for progress tracking
            if curpage == "1" or curpage == "true":                                     # If this is the first page ie first pass of the loop
                gsize = len(respd['orderedItems'])                                          # Set gsize to length of group (usually 10-12)
                try:
                    total = int(respd['totalItems'])                                    # Check for 'totalItems' element which is follower count
                except KeyError:
                    continue                                                                # Continue the loop if this DNE as it may not be over
                endpage = str(-(-total // gsize))                                           # This divison trick always rounds up the page count since x.y is always x+1
            if curpage.startswith('true'):                                              # If this is a misskey account
                i += 1                                                                      # Increment counter by one
                curpage = str(i)                                                            # Set current page to string of counter
            if total:                                                               # If there is a total (>0), use it to display progress
                print("On page: " + curpage + " out of " + endpage, end='\r')           # On page: x out of y, overwrites in terminal
            try:
                next_page = respd['next']                                           # Check for 'next' element which is the link to next page
            except KeyError:                                                            # No 'next' element means this was the last page
                break                                                                   # End the loop as there is nothing more to find
        elif resp.status_code == 429:                                           # "Too Many Requests" response code
            print("Error:  (429) - Rate limited")
            print(resp.headers)                                                     # Rate limit information is in the response headers
            wt_str = resp.headers['x-ratelimit-reset'].split(sep=".")[0]            # 'x-ratelimit-reset' is a timecode when the rate limiting will expire
            print("Current time: " + str(time.time()))
            print("Waiting until: " + wt_str)                                       # Time data after "." is not really relevant
            tt = time.strptime(wt_str, "%Y-%m-%dT%H:%M:%S")                         # Parse timecode string to time object (Year-Month-Day T Hour:Minute:Second)
            wait_til = time.mktime(tt) + 3 + utc_offset - round(time.time())        # Adjust epoch conversion from UTC to local, add 3s pad, subtract current epoch
            print("Waiting " + str(wait_til) + " seconds")                          # Result is number of seconds to wait til limit expires
            time.sleep(wait_til)                                                    # Wait for the calculated delay
        elif resp.status_code == 400:                                           # "Bad Request" response code (usually because misskey has a different format)
            print("Error:  (400) - Account may be on a misskey instance, retrying...")
            next_page = user + "/following?page=true"                               # 
        else:
            print("Error in get_followers with " + next_page)                   # All other errors handled the same
            errstring = "Error: (" + str(resp.status_code) + ") - "
            if resp.status_code == 403:                                         # "Forbidden" response code
                errstring += "User's following list is not public"
            elif resp.status_code in [404, 410, 500]:                           # "Page not found", "Gone", "User not found" response codes
                errstring += "The page is not available"
                if next_page.endswith('=1') or next_page.endswith('=true'):
                    prune.append(user)
            else:
                errstring += "Unexpected error"
            print(errstring)
            folcomp = True                                                      # End the loop as there was a serious error
    return following

# Make initial request
resp = requests.get(recfor, headers=headers)
if resp.status_code == 200:                                     # If the response was successful ie the user/link provided was valid
    # Create initial user follow list
    print("URL provided is valid")                                  # Confirm validation to the user
    start = round(time.time())                                      # Get current epoch time (Local)
    print("Start time: " + time.asctime())                          # Display current time (DotW Mon DD HH:MM:SS YYYY)
    is_dst = time.daylight and time.localtime().tm_isdst > 0        # Boolean which will be true if both checks for daylight savings pass
    utc_offset = - (time.altzone if is_dst else time.timezone)      # Altzone is offset with DST applied, timezone is without
    print("Getting User's following list...")
    rec_follow = get_followers(recfor)                              # Pass user to get_followers and receive following list
    print("User follows " + str(len(rec_follow)) + " accounts")
    print("Beginning following check...")

    # Create following-following list
    i = 1
    deep_follow = []
    sfd = {}
    for account in rec_follow:
        print("Checking account: " + str(i) + "/ " + str(len(rec_follow)))              # Keep a running count of list progress (Account X/Y)
        print("Current URL: " + account)
        if ucb and account in csfd:                                                     # If using cache and the account is already in the cache
            print("Account was found in cache: " + account + " using saved list")
            new_follow = csfd[account]                                                      # Use the existing list to save time
        else:                                                                           # If not using cache or account has not been cached
            new_follow = get_followers(account)                                             # Call get_followers and receive following list
            if sfb and len(new_follow) > 12:                                                # If saving data and the number of follows exceeds 1 standard page
                sfd[account] = new_follow                                                       # Then save the account and list for the cache
        deep_follow += new_follow                                                       # Add new follower group to total follow list
        print("Total list size: " + str(len(deep_follow)) + " (+" + str(len(new_follow)) + ")")  # Display total list size + amount added
        i += 1
        time.sleep(1)
    end = round(time.time())
    print("End time: " + time.asctime())                                            # Display current time (DotW Mon DD HH:MM:SS YYYY)
    dur = end - start                                                               # Start and End times both in local epoch, subtraction yields seconds
    dur_disp = ((dur-(dur % 3600))/3600,(((dur % 3600)-(dur % 60))/60),dur % 60)    # Parse the duration into hours, minutes, seconds
    print("Duration: " + str(int(dur_disp[0])) + " h " + str(int(dur_disp[1])) + " m " + str(dur_disp[2]) + " s")

    # Analysis
    while recfor in deep_follow:       # Check deep_follow list for occurences of the base user
        deep_follow.remove(recfor)     # Continue removing instances of themselves until there are none
    df_dict = {}                       # Create deep_follow dictionary {User: Number of occurances)
    for account in deep_follow:        # Cycle through deep_follow list
        if account not in rec_follow:       # Check that base user is not already following the account
            df_dict[account] = df_dict.get(account, 0) + 1  # If not, increment their dict entry by one
    average = len(deep_follow) / len(df_dict)   # Calculate the average follower overlap
    print("Average: " + str(len(deep_follow)) + "/" + str(len(df_dict)) + " = " + str(average))
    average = round(average)           # Display average overlap (round avg for use later)
    maximum = max(df_dict.values())    # Display maximum overlap for reference
    print("Maximum: " + str(maximum))

    # Create recommendation list
    recommend = {}
    for account in df_dict:                                                         # Cycle through deep follow dictionary
        if df_dict[account] > average:                                                  # If the account has a greater than average overlap
            recommend[account] = df_dict[account]                                           # Add that account to the recommendation list
    print("Recommended: " + str(len(recommend)) + " accounts")                      # Display the total number of recommended accounts
    sorted_recs = sorted(recommend.items(), key=lambda kv: kv[1], reverse=True)     # Order the entries by the overlap number (Reverse = Highest first)

    # Collect data on instances
    ownlist=[]
    otherlist=[]
    instances={}
    for account in sorted_recs:
        inst = account[0].split(sep='/')[2] # Pull instance name from entry
        if inst == home:                    # If instance is the user's instance
            ownlist.append(account)             # Add account to local list
        else:                               # If instance is not the same as the user
            otherlist.append(account)           # Add account to other list
        if inst not in instances.keys():    # If instance has not been seen yet
            instances[inst] = 1                 # Add instance to list of instances
        else:                               # If instance has been seen already
            instances[inst] += 1                # Increment instance count by one
    sorted_ins = sorted(instances.items(), key=lambda kv: kv[1], reverse=True) # Sort instances by frequency of occurrence like recommendation list

    # Display the prune list
    if len(prune) > 0:
        print("The following account links were unresponsive and may be completely gone. You should check to confirm.")
        print("If you get no response and the server is not just offline temporarily, consider unfollowing:")
        for account in prune:
            print(account)
    
    # Collect data on instances
    print("\nThese were the top instances for your results")
    for instance in sorted_ins[:10]:                            # Display at most 10 results
        percent = round(100*(instance[1]/len(sorted_recs)),2)       # Calculate the instance percentage
        if percent < 1.00:                                          # If the percent is less than 1
            break                                                       # Just end the display
        inststring = instance[0] + ", " + str(instance[1]) + ", " + str(percent) + "%"
        print(inststring)                                           # Display: Instance, Number, Percentage

    # Links vs Tags
    tagflag = '0'    # Predefine flag and keep in a while loop in case of bad inputs
    while tagflag not in ['L','l','link','links','T','t','tag','tags']:
        tagflag = input("Display and save results as links or tags? (L/T): ")
        if tagflag in ['L','l','link','links']:
            tagb = False
        elif tagflag in ['T','t','tag','tags']:
            tagb = True
        else:
            print("Not a valid response, try again")
    
    def link2tag(link):                # Function to turn link into a tag
        spl = link.split(sep="/")      # Split into groups by '/' -> "https"/blank/domain/"users"/username
        return spl[4] + "@" + spl[2]   # Combine last group (username) with 3rd group (domain)
    
    ### Display top results ###
    more = True
    i = j = 0                                                             # Starting from the top of the list
    k = 10                                                                # Display results in groups of 10
    while more:
        for i in range(j, k):
            if tagb:
                tag = link2tag(sorted_recs[i][0])                        # Split user URL into parts
                print(tag + " , " + str(sorted_recs[i][1]))   # Rearrange into fedi tag (abc@xyz.tld)
            else:
                print(sorted_recs[i][0] + " , " + str(sorted_recs[i][1]))                
        want = input("Display more results? (y/n): ")                     # Ask if user wants to display more results
        if want in ['y', 'Y', 'yes']:                                     # If yes, display 10 more results
            i = j = k
            k += 10
        else:                                                             # Otherwise break loop
            more = False

    ### Save data to file ###
    if sfb:                                             # Check if boolean for saving data is True
        print("Saving data to file...")
        nfilename = "Recs4 " + link2tag(recfor) + ".txt"    # Make filename based on username
        try:                                                # Try to open file for writing
            f = open(nfilename,"w+")
        except IOError:                                     # Catch errors with file input/output
            print("IO Error file could not be opened to write") # If there is an error alert the user
            SystemExit(0)                                       # Then stop the program
        print("File opened")                                # Otherwise write data to file
        f.write("Data for: " + recfor + "\n")               # Begin with base user
                         # And their following list

        if ucb:
            f.write("rec_fol = [")          # Record the main user's following list for the cache
            i=0
            for acct in rec_follow:
                f.write("'" + acct + "'")
                if acct != rec_follow[-1]:  # Check for last list entry to not include a trailing comma, space, line
                    f.write(", ")
                    if i > 0 and i % 3 == 0:    # Split up list after a few entries to prevent huge lines
                        f.write("\n")               # Nested within last entry check to avoid trailing newline
                i += 1
            f.write("]\n\n\n")
            f.write("rec_sfd = {")          # Record the checked users and their following lists for the cache
            i = 0
            for entry in sfd:
                f.write("'" + entry + "':\n[")
                for acct in sfd[entry]:
                    f.write("'" + acct + "'")
                    if acct != sfd[entry][-1]:
                        f.write(", ")
                        if i > 0 and i % 3 == 0:
                            f.write("\n")
                    i += 1
                f.write("],\n\n")
            f.write("}")
            f.write("\n\n\n")
        
        # Ask whether to split the data into 2 groups: own server vs all other servers
        split_data = input('Do you want to separate users from your instance to their own list? (y/n): ')
        if split_data in ['y', 'Y', 'yes']:
            spb = True
            print("Data will be split")
        else:
            spb = False
            print("Data will not be split")
        
        f.write("\n***RECOMMENDATIONS***\n")
        if spb:  # If user opted to split up the results then save from own instance first, all others after
            f.write("\n*Users from your instance*\n")
            for account in ownlist:
                if tagb:                                        # If the user requested tags instead of links
                    tag = link2tag(account[0])                      # Split user URL into parts
                    f.write(tag + "," + str(account[1]) + "\n")     # Rearrange into fedi tag (abc@xyz.tld)
                else:                                           # If the user requested links instead of tags
                    f.write(account[0] + " , " + str(account[1]) + "\n") # Save as actor URL instead of tag
            f.write("\n\n*Users from other instances*\n")
            for account in otherlist:
                if tagb:
                    f.write(link2tag(account[0]) + "," + str(account[1]) + "\n")
                else:
                    f.write(account[0] + " , " + str(account[1]) + "\n")
        else:  # Otherwise save results in original order from the complete recommendation list
            for account in sorted_recs:
                if tagb:
                    tag = link2tag(account[0])
                    f.write(tag + "," + str(account[1]) + "\n")
                else:
                    f.write(account[0] + " , " + str(account[1]) + "\n")

        f.close() #Close file
        print('Successfully saved to file: ' + nfilename)
else:
    print("User data provided was invalid, try again")
